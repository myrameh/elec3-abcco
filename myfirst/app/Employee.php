<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //ADD VALIDATION
    protected $primaryKey = 'empid';
    public $timestamps = false;
    public $incrementing = false;

    public function getDepartmentName(){
        return $this->hasOne('App\Department','deptid','empdeptid');        
    }
    public function getPositionName(){
        return $this->hasOne('App\Position','posid','empposid');  
    }

    public function getEmployeeValidationRules($type){
        if($type=='insert')
            return self::$rules;
        elseif($type=='update'){
            self::$rules= array_shift(self::$rules);
            //dd(self::$rules);
            return $rules;
        }
    }
}
