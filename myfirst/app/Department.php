<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $primaryKey = 'deptid';
    public $timestamps = false;
    public $incrementing = false;
}
