<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PositionController extends Controller
{
    //
    public function newPosition(){

    }
    public function saveNewPosition(Request $request){

    }
    public function updatePositionInfo(){

    }
    public function savePositionUpdates($posid,Request $request){

    }
    public function listPositions($mode,Request $request){

    }
    public function markPosition($posid){

    }
    public function removePosition($mode,Request $request){

    }
}
