<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    //
    public function getData(){
        return view('getdata');
    }

    public function gatherData(Request $request){
         // dd($request);
        $values = [];
        $values[] = $request->data1;
        $values[] = $request->data2;
        $values[] = $request->data3;

        // return view('getdata',compact('values'));
        return redirect()->back()->withInput()->with(compact('values'));
    }
}
