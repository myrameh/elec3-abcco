<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    //
    public function newDeparment(){

    }
    public function saveNewDeparment(Request $request){

    }
    public function updateDeparmentInfo(){

    }
    public function saveDeparmentUpdates($deptid,Request $request){

    }
    public function listDeparments($mode,Request $request){

    }
    public function markDeparment($deptid){

    }
    public function removeDeparment($mode,Request $request){

    }
}
