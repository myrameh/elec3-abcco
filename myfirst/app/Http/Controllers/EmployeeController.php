<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Department;
use App\Position;

class EmployeeController extends Controller
{
    //ADD VALIDATION RULES
    public function newEmployee(){
        $allDepartments = Department::all();
        $allPositions = Position::all();
        return view('employees.newemployee',compact('allDepartments','allPositions'));
    }
    public function saveNewEmployee(Request $request){
        $newEmployee = new Employee;
        $newEmployee->empid = $request->empid;
        $newEmployee->empfirstname = $request->empfirstname;
        $newEmployee->empmidname = $request->empmidname;
        $newEmployee->emplastname = $request->emplastname;
        $newEmployee->empdeptid = $request->empdeptid;
        $newEmployee->empposid = $request->empposid;
        
        $newEmployee->save();

        return redirect()->to(url('employee/list').'?mode=list');
    }
    public function updateEmployeeInfo($empid){
        $allDepartments = Department::all();
        $allPositions = Postion::all();
        $employeeRecord = Employee::find($empid);

        return view();

    }
    public function saveEmployeeUpdates($empid,Request $request){
        $validation = Validator::make($request->input(),Employee::geEmployeeValidationRules, Employee::getValidationMessages);

        if($validation->passes()){
            $employeeRecord = Employee::find($empid);
            $empoyeeRecord->empfirstname = $request->empfirstname;
            $empoyeeRecord->empmidname = $request->empmidname;
            $empoyeeRecord->emplastname = $request->emplastname;
            $empoyeeRecord->empdeptid = $request->empdeptid;
            $empoyeeRecord->empposid = $request->empposid;

            $employeeRecord->save();
            return redirect()->to(url('/employee/list').'?mode=list');
        } else {
            return redirect()->back()->withInput()->withErrors($validation);
        }
    }
    public function listEmployees(Request $request){
        $allEmployees = Employee::orderBy('emplastname')->paginate(8);
        
        if($request->mode=='list'){
            return view('employees.listemployees',compact('allEmployees'));
        }elseif($request->mode =='remove'){
            $employeeRecord = Employee:: find($request->empid);

            if($employeeRecord){
                $empfirstname = $employeeRecord->empfirstname;
                $emplastname = $employeeRecord->emplastname;
            }else{
                $empfirstname = null;
                $emplastname = null;
            }
        }
    }
    public function markEmployee($empid,$page){
        return redirect()->to(url('/employee/list').'?mode=remove');
    }
    public function removeEmployee($mode,Request $request){
        
    }

}
