<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=form, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="{{ url('/getdata') }}" method="post">
        {{ csrf_field() }}
        Data 1: <input type="text" name="data1" value="{{ old('data1') }}"><br>
        Data 2: <input type="text" name="data2" value="{{ old('data2') }}"><br>
        Data 3: <input type="text" name="data3" value="{{ old('data3') }}"><br>
        <button>
            Submit
        </button>
    </form>
    <section>
        @if(Session::has($values))
            <ul>
                @foreach($values as $value)
                    <li>{{ $value }}</li>
                @endforeach
            </ul>
        @endif
    </section>
</body>
</html>