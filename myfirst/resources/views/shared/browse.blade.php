<section>
    <section class="row">
        <section class="col-md-12">
            <h3>List Employees</h3>
        </section>
    </section>
    <section class="row">
        <section class="col-md-12">
            <a href=" {{ url('/employee/new/entry') }}">Add New Employee</a>
        </section>
    </section>
        <section class="row">
        <section class="col-md-12"style="height: 80vh;">
            <table class="table table-hover" >
                <thead>
                    <tr>
                        <th>Employee No.</th>
                        <th>Employee Name</th>
                        <th>Department</th>
                        <th>Position</th>
                        <th>Action</th>
                    </tr>
                    <tbody>
                        @foreach($allEmployees as $employee)
                            <tr>
                                <td>{{ $employee->empid }}</td>
                                <td>
                                    @if( $employee->empmidname == null)
                                        {{ $employee->emplastname.', '.$employee->empfirstname }}
                                    @else
                                        {{ $employee->emplastname.', '.$employee->empfirstname.' '.substr($employee->empmidname,0,1).'.'  }}
                                    @endif
                                </td>
                                <td>{{ $employee->getDepartmentName->deptname }}</td>
                                <td>{{ $employee->getPositionName->posname }}</td>
                                <td>
                                    <a><img src="{{ asset('svg/pencil.svg') }}" style="width:15px;"></a>&nbsp;&nbsp;
                                    <a href="{{ url('employee'.'/'.$employee->empid.'/'.$allEmployees->currentPage().'/mark') }}"><img src="{{ asset('svg/trash.svg') }}" style="width:15px;"></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </thead>
            </table>    
        </section>
        <section>{{ $allEmployees->appends([])->links() }}</section>
    </section>
        
</section>