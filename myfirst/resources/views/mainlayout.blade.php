<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title_text')</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script type="javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script type="javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
    <script type=javascript src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript"></script>
    @yield('js_codes')
</head>
<body>
    <section class="row">
        <section class="col-md-12">
        <header>
            <h1>
                <span style="font-family:'Lucida Sans','Lucida Sans Regular'">
                    &emsp;ABBCO HRIS (Human Resource Information System)
                </span>
            </h1>
        </header>
        </section>
    </section>
    <section class="row">
            <section class="col-md-3">
                @include('mainnav')
            </section>
            <section class="col-md-9">
                @yield('content_output')
            </section>    
    </section>
</body>
</html>