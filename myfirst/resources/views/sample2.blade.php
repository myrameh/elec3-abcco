<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="{{ url('/sample2') }}" method="post">
        {{ csrf_field() }}
        Full Name: <input type="text" name="name"><br>
        Age: <input type="text" name="age"><br>
        Username: <input type="text" name="username"><br>
        <button>
            Register
        </button>
    </form>
    <section>
        @if(isset($values))
            <ul>
                @foreach($values as $value)
                    <li>{{ $value }}</li>
                @endforeach
            </ul>
        @endif
    </section>
</body>
</html>