@extends('../mainlayout')

@section('title_text')
    Update Employee Information
@endsection
   
<!-- @section('js_codes')
    <script type="javascript">
        $(function(){
            $('#btnExit').on('click',function(e){
                e.preventDefault();
                window.location.assign("{{ url('/employee/list').'?mode=list' }}");
            });
        });
    </script>
@endsection -->


@section('js_codes')
        $('#btnExit').on('click',function(e)){
            e.preventDefaul();
            window.location.assign("{{ url('/employee/list').'?mode=list' }}" + '&page='+ {{ $page }} );
        }
@endsection

@section('content_output')
    <section>
        <section class="row">
            <section class="col-md-12">
                <h3>Add New Employee</h3>
            </section>
        </section>
        <section class="row">
            <section class="col-md-12">
                <form action="{{ url('/employee/new/entry') }}" method="post">
                    {{ csrf_field() }}
                    <section class="row">
                        <section class="col-md-6">
                            <section class="form-group">
                                <label for="">ID No.</label>
                                <input type="text" name="empid" class="form-control" value="{{ old('empid') }}" readonly>
                            </section>
                            <section class="form-group">
                                <label for="">First Name</label>
                                <input type="text" name="empfirstname" class="form-control" value="{{ old('empfirstname') }}">
                            </section>
                            <section class="form-group">
                                <label for="">Middle Name</label>
                                <input type="text" name="empmidname" class="form-control" value="{{ old('empmidname') }}">
                            </section>
                            <section class="form-group">
                                <label for="">Last Name</label>
                                <input type="text" name="emplastname" class="form-control" value="{{ old('emplastname') }}">
                            </section>
                        </section>
                        <section class="col-md-6">
                            
                                <section class="col-md-6">
                                    <select name="empdeptid" id="" class="form-control">
                                        @foreach($allDepartments as $department)
                                            <option value="{{ $department->deptid }}">{{ $department->deptname }}</option>
                                        @endforeach
                                    </select>
                                    <select name="empposid" id="" class="form-control">
                                        @foreach($allPositions as $position)     
                                            <option value="{{ $position->posid }}">{{ $position->posname }}</option>
                                        @endforeach
                                    </select>
                                </section>
                                
                                <section class="col-md-6">
                                    <section class="form-group">
                                        <button class="form-control btn btn-primary">Save</button>
                                        <button class="form-control btn btn-danger" id="btnExit">Exit</button>
                                    </section>
                                </section>
                        </section>
                    </section>
                </form>
            </section>
        </section>
    </section>
@endsection