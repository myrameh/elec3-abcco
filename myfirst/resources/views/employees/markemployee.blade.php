@extends('../mainlayout')

@section('title_text')
    Mark a Record for Deletion
@endsection

@section('js_codes')
    $('#markEmployeeModal').modal({
        backdrop:static, 
        keyboard:true,
    });

    $('#deleteEmployee').on('click',function(e){
        e.preventDefault();
        window.location.assign(" url('/employee'.$empid.'/'.$page.'/delete') ");
    });
@endsection

@section('content_output')
    @include('shared.browse')
    <div class="modal fade" id="markEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Warning!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>Employee First Name:</span><span>{{ $empfirstname }}</span><br>
                    <span>Employee Last Name:</span><span>{{ $emplastname }}</span><br>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="deleteEmployee">Proceed</button>
                </div>
            </div>
        </div>
    </div>
@endsection