@extends('../mainlayout')

@section('title_text')
    List ABCCO Employees
@endsection

@section('content_output')
    <section class="col-md-12" style="height: 75vh; background-color: lemonchiffon">
        @include('shared.browse')
    </section>
@endsection
