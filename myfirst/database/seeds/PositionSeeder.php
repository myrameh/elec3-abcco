<?php

use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $positions = [
            [
                'posid' => 0,
                'posname' => 'System Administrator',
                'posdescription' => null
            ],
            [
                'posid' => 1,
                'posname' => 'General Manager',
                'posdescription' => null
            ],
            [
                'posid' => 2,
                'posname' => 'Branch Manager',
                'posdescription' => null
            ],
            [
                'posid' => 3,
                'posname' => 'IT Manager',
                'posdescription' => null
            ],
            [
                'posid' => 4,
                'posname' => 'Sales Manager',
                'posdescription' => null
            ],
            [
                'posid' => 5,
                'posname' => 'Accounting Manager',
                'posdescription' => null
            ],
            [
                'posid' => 6,
                'posname' => 'Marketing Manager',
                'posdescription' => null
            ],
            [
                'posid' => 7,
                'posname' => 'Production Manager',
                'posdescription' => null
            ],
            [
                'posid' => 8,
                'posname' => 'Comptroller',
                'posdescription' => null
            ],
            [
                'posid' => 9,
                'posname' => 'Chief Accountant',
                'posdescription' => null
            ],
            [
                'posid' => 10,
                'posname' => 'Bookkeeper',
                'posdescription' => null
            ],
            [
                'posid' => 11,
                'posname' => 'Cashier',
                'posdescription' => null
            ],
            [
                'posid' => 12,
                'posname' => 'Developer',
                'posdescription' => null
            ],
            [
                'posid' => 13,
                'posname' => 'Clerk',
                'posdescription' => null
            ],
            [
                'posid' => 14,
                'posname' => 'Janitor',
                'posdescription' => null
            ],
            [
                'posid' => 15,
                'posname' => 'Receptionist',
                'posdescription' => null
            ],
            [
                'posid' => 16,
                'posname' => 'Secretary',
                'posdescription' => null
            ],
        ];
        DB::table('positions')->insert($positions);
    }
}
