<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $employees = [
            [
                'empid' => 'EMP0',
                'empfirstname' => 'Juan',
                'emplastname' => 'Dela Cruz',
                'empmidname' => '',
                'empdeptid' => 0,
                'empposid' => 0
            ],
            [
                'empid' => 'EMP1',
                'empfirstname' => 'Nana',
                'emplastname' => 'Alucard',
                'empmidname' => '',
                'empdeptid' => 1,
                'empposid' => 2
            ],
            [
                'empid' => 'EMP2',
                'empfirstname' => 'Myra',
                'emplastname' => 'Pelostratos',
                'empmidname' => '',
                'empdeptid' => 2,
                'empposid' => 3
            ],
            [
                'empid' => 'EMP3',
                'empfirstname' => 'Jay Ann',
                'emplastname' => 'Dela Cerna',
                'empmidname' => '',
                'empdeptid' => 6,
                'empposid' => 3
            ],
            [
                'empid' => 'EMP4',
                'empfirstname' => 'Luna',
                'emplastname' => 'Pelostratos',
                'empmidname' => '',
                'empdeptid' => 4,
                'empposid' => 8
            ],
            [
                'empid' => 'EMP5',
                'empfirstname' => 'Schiezka',
                'emplastname' => 'Dela Victoria',
                'empmidname' => '',
                'empdeptid' => 5,
                'empposid' => 13
            ],
            [
                'empid' => 'EMP6',
                'empfirstname' => 'Helvi',
                'emplastname' => 'Tagacanao',
                'empmidname' => '',
                'empdeptid' => 2,
                'empposid' => 16
            ],
            [
                'empid' => 'EMP7',
                'empfirstname' => 'Chrislene',
                'emplastname' => 'Tulabing',
                'empmidname' => '',
                'empdeptid' => 5,
                'empposid' => 9
            ],
            [
                'empid' => 'EMP8',
                'empfirstname' => 'Joseph',
                'emplastname' => 'Magabilin',
                'empmidname' => '',
                'empdeptid' => 4,
                'empposid' => 8
           ],
            [
                'empid' => 'EMP9',
                'empfirstname' => 'Louie',
                'emplastname' => 'Alolor',
                'empmidname' => '',
                'empdeptid' => 1,
                'empposid' => 11
            ]
        ];
        DB::table('employees')->insert($employees);
    }
}
