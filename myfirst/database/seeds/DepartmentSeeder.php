<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $departments = [
            [
                'deptid' => 0,
                'deptname' => 'Administrators'
            ],
            [
                'deptid' => 1,
                'deptname' => 'Executive'
            ],
            [
                'deptid' => 2,
                'deptname' => 'Accounting'
            ],
            [
                'deptid' => 3,
                'deptname' => 'Sales'
            ],
            [
                'deptid' => 4,
                'deptname' => 'Marketing'
            ],
            [
                'deptid' => 5,
                'deptname' => 'Production'
            ],
            [
                'deptid' => 6,
                'deptname' => 'IT'
            ],
        ];
        DB::table('departments')->insert($departments);
    }
}
