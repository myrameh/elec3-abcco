<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/sample', function () {
//     $sampleText= "This is coming from a closure";

//     return view('sample')->with('sample',$sampleText);
//     //return view('sample',['sample'=>$sampleText]);
// });

// Route::get('/getdata','MyController@getData');
// Route::post('/getdata','MyController@gatherData');

// Route::get('/sample2',function(){
//     return view('sample2');
// });

// Route::post('/sample2',function(Request $request){
//     // dd($request);
//     $values = [];
//     $values[] = $request->name;
//     $values[] = $request->age;
//     $values[] = $request->username;

//     return view('sample2',compact('values'));
// });

Route::get('/main',function(){
    return view('mainlayout');
});

Route::get('/employee/list','EmployeeController@listEmployees');

Route::get('/employee/new/entry','EmployeeController@newEmployee');
Route::post('/employee/new/entry','EmployeeController@saveNewEmployee');

Route::get('/employee/{empid}/update','EmployeeController@updateEmployeeInfo');
Route::post('/employee/{empid}/update','EmployeeController@updateEmployeeInfo');

Route::get('/employee/{empid}/{page}/mark','EmployeeController@markEmployee');
Route::get('/employee/{empid}/{page}/delete','EmployeeController@removeEmployee');

